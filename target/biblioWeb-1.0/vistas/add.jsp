<%-- 
    Document   : add
    Created on : Nov 30, 2021, 3:12:04 AM
    Author     : Brigitte
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/estilos.css"><!-- comment -->
        <title>Nuevo Libro</title>
    </head>
   <body>
        <div class="container">
            <header class="main-header">                
                <nav class="main-nav">
                    <ul>
                        <li><a href="index.jsp">SALIR</a></li>
                        <li><a href="menuLibros.jsp">REGRESAR</a> </li>  
                    </ul>
                </nav>
            </header>
            
            <div>
                <h1>Nuevo Libro</h1>
                <form action="ControladorLibros" method="post">
                    CodigoLib:<br>
                    <input type="text" name="txtCodLib"><br>
                    Nombre: <br>
                    <input type="text" name="txtNombre"><br>
                    Estado: <br>
                    <input type="text" name="txtEstado"><br>
                    Anio: <br>
                    <input type="text" name="txtAnio"><br>
                    Categoria: <br>
                    <input type="text" name="txtCategoria"><br>
                    Editorial: <br>
                    <input type="text" name="txtEditorial"><br>
                    CantEjemplar: <br>
                    <input type="text" name="txtCantEjemplar"><br>

                    <input  type="submit" name="accion" value="Agregar">                    
                </form>
            </div>   
                       

        </div>
    </body>
</html>

