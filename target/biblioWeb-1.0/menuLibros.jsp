<%-- 
    Document   : menuLibros
    Created on : Nov 28, 2021, 3:23:53 PM
    Author     : Brigitte
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link rel="stylesheet" type="text/css" href="css/estilos.css">
          <title>Menu Libros</title>
    </head>
   <body>
        <div class="container">

            <!-- cabecera -->
             <h1>¡Hola ${nombre} ${apellido}!</h1>
            <h1>Libros</h1>
            <h3>Administrador</h3>
            <header class="main-header">
                <center><img src="images/body2.jpg" width="80" height="80" alt="logotipo" class="logo"></center>
                    <nav class="main-nav">
                        <ul>
                            <li><a href="index.jsp">SALIR</a></li>
                        </ul>
                        <ul>
                            <li><a href="ControladorLibros?accion=listar">Listado de Libros</a></li>
                            <li><a href="ControladorLibros?accion=add">Agregar Nuevo Libro</a> </li>
                            <li><a href="ControladorLibros?accion=listarActual">Actualizar Libro</a></li>                        
                        </ul>
                    </nav>
            </header>            
            <a href="menuUsuarios.jsp">Usuarios</a>
        </div>
    </body>
</html>
