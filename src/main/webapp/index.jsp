<%-- 
    Document   : index
    Created on : Nov 28, 2021, 2:58:34 PM
    Author     : Brigitte
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"><!-- comment -->
        <title>Start Page</title>
    </head>
   <body>
    <h1>Login</h1>
    <form method="post" action="ControladorLogin">
      <input type="text" name="usuario" placeholder="Usuario" required >
      <input type="password" name="password" placeholder="Password" required>
      <input type="hidden" name="dirIP" value="localhost">
      <input type="hidden" name="nomBD" value="bibliotecaweb">
      <input type="submit" value="Ingresar">
    </form>
  </body>
</html>
