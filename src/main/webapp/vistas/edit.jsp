<%-- 
    Document   : edit
    Created on : Nov 30, 2021, 2:28:09 AM
    Author     : Brigitte
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/estilos.css"><!-- comment -->
        <title>Actualizar Libro</title>
    </head>
    <body>
        <div class="container">
            <div class="col-lg-6">

<form action="ControladorLibros" method="post">
                Nombre:<br>
                <input type="text" name="txtNombre" value="${libro.getNombre()}"><br>
                Estado: <br>
                <input type="text" name="txtEstado" value="${ libro.isEstado()}"><br>
                Anio:<br>
                <input type="text" name="txtAnio" value="${ libro.getAnio()}"><br>
                Categoria: <br>
                <input type="text" name="txtCategoria" value="${libro.getCategoria()}"><br>
                Editorial:<br>
                <input  type="text" name="txtEditorial" value="${ libro.getEditorial()}"><br>
                Cantejemplar: <br>
                <input  type="text" name="txtCantEjemplar" value="${libro.getCantEjemplar()}"><br>


                <input type="hidden" name="txtIdLibro" value="${ libro.getCodLib()}">                
                <input type="submit" name="accion" value="Actualizar">                 
                <a href="ControladorLibros?accion=listarActual">Regresar</a>
                </form>
            </div>
        </div>

    </body>
</html>

