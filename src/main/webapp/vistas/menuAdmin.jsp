<%-- 
    Document   : menuAdmin
    Created on : Nov 28, 2021, 3:17:14 PM
    Author     : Brigitte
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu Libros</title>
    </head>
    <body>
         <h1>¡Hola ${nombre} ${apellido}!</h1>
      <h1>Bienvenido a la biblioteca Nacional!</h1>
        <center><img src="../images/body2.jpg" width="80" height="80" alt="logotipo" class="logo"></center>
        <h3>Administrador</h3>
        <div class="container">
            <!-- cabecera -->            
            <header class="main-header">            
                    <nav class="main-nav">
                        <ul>
                            <li><a href="../index.jsp">SALIR</a></li>
                        </ul>
                        <ul>
                            <li><a href="../menuLibros.jsp">Libros</a></li>
                            <!-- <li><a href="../menuUsuarios.jsp">Usuarios</a> </li>-->  
                            <li><a href="../menuUsuarios.jsp">Usuarios</a> </li>
                        </ul>
                    </nav>
            </header>
        </div>
    </body>
</html>
