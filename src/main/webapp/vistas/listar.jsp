<%-- 
    Document   : listar
    Created on : Nov 28, 2021, 5:58:06 PM
    Author     : Brigitte
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <title>Biblioteca Nacional</title>
    </head>    
    <body>
        <div class="container">
            <header class="main-header">                
                <nav class="main-nav">
                    <ul>
                        <li><a href="index.jsp">SALIR</a></li>
                        <li><a href="menuLibros.jsp">REGRESAR</a> </li>  
                        <!-- <li><a href="menuLibros.jsp">Libros</a> </li>  
                        <li><a href="vistas/menuAdmin.jsp">Administracion</a> </li> -->
                    </ul>
                </nav>
            </header>

            <h1>¡Hola ${nombre} ${apellido}!</h1>
            <div>
                <h1>Listado de Libros</h1>          

                <table border="1">
                    <thead>
                        <tr>
                            <th>ID LIBRO</th>
                            <th>NOMBRE</th>
                            <th>ESTADO</th>
                            <th>ANIO</th>
                            <th>CATEGORIA</th>
                            <th>EDITORIAL</th>
                            <th>CANTEJEMPLAR</th>                             
                        </tr>
                    </thead>
                    <c:forEach var="libro" items="${libros}" >
                        <tr>
                            <td>${libro.getCodLib()}</td>
                            <td>${libro.getNombre()}</td>
                            <td>${libro.isEstado()}</td>
                            <td>${libro.getAnio()}</td>
                            <td>${libro.getCategoria()}</td>
                            <td>${libro.getEditorial()}</td>
                            <td>${libro.getCantEjemplar()}</td>                      
                        </tr>                           
                    </c:forEach>
                </table>
            </div>          
        </div>
    </body>
</html>

