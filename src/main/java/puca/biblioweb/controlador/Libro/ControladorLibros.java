/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puca.biblioweb.controlador.Libro;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import misClases.Libro;
import puca.biblioweb.modelo.LibroDAO;

/**
 *
 * @author Brigitte
 */
@WebServlet(name = "ControladorLibros", urlPatterns = {"/ControladorLibros"})
public class ControladorLibros extends HttpServlet {

    String listar = "vistas/listar.jsp";
    String listarActual = "vistas/actualizar.jsp";
    String add = "vistas/add.jsp";
    String edit = "vistas/edit.jsp";
    Libro l = new Libro();
    LibroDAO dao = new LibroDAO();
    int id;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try ( PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet ControladorLibros</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet ControladorLibros at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
//        List<String> cervezasList = new ArrayList<>();
//        cervezasList.add("rubia");
//        cervezasList.add("negra");
//        request.setAttribute("cervezas", cervezasList);

        String acceso = "";
        String action = request.getParameter("accion");
        if (action.equalsIgnoreCase("listar")) {
//            LibroDAO libroDAO = new LibroDAO();
//            List<Libro> librosList = libroDAO.listar();

            request.setAttribute("libros",  listLibros());
            acceso = listar;
        } else if (action.equalsIgnoreCase("listarActual")) {
//            LibroDAO libroDAO = new LibroDAO();
//            List<Libro> librosList = libroDAO.listar();
//            request.setAttribute("libros", librosList);
 request.setAttribute("libros",  listLibros());
            acceso = listarActual;
        } else if (action.equalsIgnoreCase("add")) {            
            acceso = add;
        } else if (action.equalsIgnoreCase("Agregar")) {
            String codLib = request.getParameter("txtCodLib");//*
            String nombre = request.getParameter("txtNombre");
            String estado = request.getParameter("txtEstado");
            String anio = request.getParameter("txtAnio");
            String categoria = request.getParameter("txtCategoria");
            String editorial = request.getParameter("txtEditorial");
         String cantEjemplar = request.getParameter("txtcantEjemplar");

            int cod = Integer.parseInt(codLib);
            boolean e = Boolean.parseBoolean(estado);
            int a = Integer.parseInt(anio);
            // int cantEj= Integer.parseInt(cantEjemplar);

            l.setCodLib(cod);
            l.setNombre(nombre);
            l.setEstado(e);
            l.setAnio(a);
            l.setCategoria(categoria);
            l.setEditorial(editorial);
            l.setCantEjemplar(100);
            dao.add(l);
            request.setAttribute("libros",  listLibros());
            acceso = listar;
        } else if (action.equalsIgnoreCase("editar")) {
            id = Integer.parseInt(request.getParameter("id"));
            LibroDAO dao = new LibroDAO();
//            int id = Integer.parseInt((String) request.getAttribute("idlib"));
// int id = Integer.parseInt((String) request.getParameter("idlib"));
            Libro libro = (Libro) dao.list(id);
            //   int idlib = libro.getCodLib();
           // request.setAttribute("libro", request.getParameter("id"));
            request.setAttribute("libro", libro);
            acceso = edit;
        } else if (action.equalsIgnoreCase("Actualizar")) {
            id = Integer.parseInt(request.getParameter("txtIdLibro"));
           //  id = Integer.parseInt((String) request.getAttribute("txtIdLibro"));

            String nombre = request.getParameter("txtNombre");
            String estado = request.getParameter("txtEstado");
            int anio = Integer.parseInt(request.getParameter("txtAnio"));
            String categoria = request.getParameter("txtCategoria");
            String editorial = request.getParameter("txtEditorial");
            int cantEjemplar = Integer.parseInt(request.getParameter("txtCantEjemplar"));

            boolean e = Boolean.parseBoolean(estado);

            l.setCodLib(id);
            l.setNombre(nombre);
            l.setEstado(e);
            l.setAnio(anio);
            l.setEditorial(editorial);
            l.setCategoria(categoria);
            l.setCantEjemplar(cantEjemplar);

            dao.edit(l);
            request.setAttribute("libros",  listLibros());
            
            acceso = listarActual;
        } else if (action.equalsIgnoreCase("eliminar")) {
            id = Integer.parseInt(request.getParameter("id"));
            l.setCodLib(id);
            dao.eliminar(id);
            request.setAttribute("libros",  listLibros());
            
              acceso = listarActual;
        }
        

   

    RequestDispatcher vista = request.getRequestDispatcher(acceso);
    vista.forward (request, response);

}
 public List<Libro> listLibros() {
        LibroDAO libroDAO = new LibroDAO();
        List<Libro> librosList = libroDAO.listar();
        return librosList;
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
/**
 * Handles the HTTP <code>GET</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
