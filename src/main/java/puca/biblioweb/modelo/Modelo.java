package puca.biblioweb.modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Modelo {

    private String jdbcDriver;
    private String urlRoot;
    private String dbName;
    private String nombreEncontrado;
    private String apellidoEncontrado;
    private String rolEncontrado;

    public Modelo(String ip, String bd) {
        jdbcDriver = "com.mysql.cj.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + ip + "/";
        dbName = bd;
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            //reportException(e.getMessage());
        }
    }

    public boolean consultarPorUserPassword(String nomUsuario, String contrasenya) {
        boolean resultado = false;
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
//             Connection con = DriverManager.getConnection(urlRoot + dbName, "root", "admin");
            Statement stmt = con.createStatement();
            stmt.execute("SELECT nombre,apellido,perfil FROM usuarios WHERE username='" + nomUsuario
                    + "' AND password='" + contrasenya + "';");
            ResultSet rs = stmt.getResultSet();
            if (rs.next()) {
                nombreEncontrado = rs.getString(1);
                apellidoEncontrado = rs.getString(2);
                rolEncontrado = rs.getString(3);
                resultado = true;
            }
            con.close();
        } catch (SQLException e) {
            //reportException(e.getMessage());
        }
        return resultado;
    }

    public String obtenerNombre() {
        return nombreEncontrado;
    }

    public String obtenerApellido() {
        return apellidoEncontrado;
    }

    public String obtenerRol() {
        return rolEncontrado;
    }
}
