/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puca.biblioweb.modelo;

import conexio.Conexion;
import crud.CrudLibro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import misClases.Libro;

/**
 *
 * @author Brigitte
 */
public class LibroDAO implements CrudLibro{
    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Libro l = new Libro();
    
    private ArrayList<Libro> libros;

    public LibroDAO() {        
        this.libros = new ArrayList<>();
    }
    
    public void setLibros(ArrayList<Libro> libros) {
        this.libros = libros;
    }
    public ArrayList<Libro> getLibros() {
        return libros;
    }
    public void agregarLibro(Libro l) {
        libros.add(l);
    }

    @Override
    public List listar() {
        ArrayList<Libro> list = new ArrayList<>();
        String sql = "SELECT * FROM libro";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Libro lib = new Libro();
                lib.setCodLib(rs.getInt("Id"));
                lib.setNombre(rs.getString("nombre"));
                lib.setEstado(rs.getBoolean("estado"));
                lib.setAnio(rs.getInt("anio"));
                lib.setCategoria(rs.getString("categoria"));
                lib.setEditorial(rs.getString("editorial"));
                lib.setCantEjemplar(rs.getInt("cantEjemplar"));

                list.add(lib);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    @Override
    public Libro list(int id) {
        String sql = "SELECT * FROM libro WHERE Id=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                l.setCodLib(rs.getInt("Id"));
                l.setNombre(rs.getString("nombre"));
                l.setEstado(rs.getBoolean("estado"));
                l.setAnio(rs.getInt("anio"));
                l.setCategoria(rs.getString("categoria"));
                l.setEditorial(rs.getString("editorial"));
                l.setCantEjemplar(rs.getInt("cantEjemplar"));

            }
        } catch (SQLException e) {
        }
        return l;
    }

    @Override
    public boolean add(Libro lib) {

        // String sql = "insert into libro(nombre, estado, anio,categoria, editorial,cantEjemplar)values('" + lib.getNombre() + "',''" + lib.isEstado() + "'," + lib.getAnio() + ",'" + lib.getCategoria() + "','" + lib.getEditorial() + "'," + lib.getCantEjemplar() + ")"; 
        String sql = "INSERT INTO libro(Id,nombre,estado,anio,categoria,editorial,cantEjemplar)VALUES(" + lib.getCodLib() + ",'" + lib.getNombre() + "'," + lib.isEstado() + "," + lib.getAnio() + ",'" + lib.getCategoria() + "','" + lib.getEditorial() + "'," + lib.getCantEjemplar() + ")";
        // String sql = "insert into libro values(" + lib.getCodLib() + ",'" + lib.getNombre() + "',''" + lib.isEstado() + "'," + lib.getAnio() + ",'" + lib.getCategoria() + "','" + lib.getEditorial() + "'," + lib.getCantEjemplar() + ")";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(LibroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean edit(Libro lib) {        
      //  String sql="update libro set nombre='"+lib.getNombre()+"',estado='"+lib.isEstado()+"',anio='"+lib.getAnio()+"',categoria='"+lib.getCategoria()+"',editorial='"+lib.getEditorial()+"',cantEjemplar='"+lib.getCantEjemplar()+"'where Id="+lib.getCodLib();
      //  String sql = "UPDATE libro SET nombre=' " + lib.getNombre() + "' ' ,estado=' '" + lib.isEstado() + "' ,,anio=' '" + lib.getAnio() + "'' ,categoria=' '" + lib.getCategoria() + "'' ,editorial=' '" + lib.getEditorial() + "'' ,cantEjemplar=' '" + lib.getCantEjemplar() + "'' WHERE Id='" + lib.getCodLib();
       String sql = "UPDATE libro SET nombre='" + lib.getNombre() + "',estado='" + lib.isEstado() + "',anio='" + lib.getAnio() + "',categoria='" + lib.getCategoria() + "',editorial='" + lib.getEditorial() + "',cantEjemplar='" + lib.getCantEjemplar() + "'where Id=" + lib.getCodLib();
     
      try {
            con = cn.getConnection();      
            ps = con.prepareStatement(sql); 
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;
    }

    @Override
    public boolean eliminar(int id) {
        String sql = "DELETE FROM libro WHERE Id=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
    
    ///Idem l    public Libro list(int id) .....
    public Libro buscaLibro(int id) {
        String sql = "SELECT * FROM libro WHERE Id=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                l.setCodLib(rs.getInt("Id"));
                l.setNombre(rs.getString("nombre"));
                l.setEstado(rs.getBoolean("estado"));
                l.setAnio(rs.getInt("anio"));
                l.setCategoria(rs.getString("categoria"));
                l.setEditorial(rs.getString("editorial"));
                l.setCantEjemplar(rs.getInt("cantEjemplar"));

            }
        } catch (SQLException e) {
        }
        return l;
    }

    
}
