/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misClases;

/**
 *
 * @author Brigitte
 */
public class Libro {

   // private static int ID = 0;
    int codLib;
    String nombre;
    boolean estado;
    int anio;
    //Autor autor;
    String categoria;
    String editorial;
    int cantEjemplar;

    public Libro() {
    }

    public Libro(int codLib, String nombre, boolean estado, int anio, Autor autor, String categoria, String editorial, int cantEjemplar) {
        this.codLib = codLib;
        this.nombre = nombre;
        this.estado = estado;
        this.anio = anio;
       // this.autor = autor;
        this.categoria = categoria;
        this.editorial = editorial;
        this.cantEjemplar = cantEjemplar;

    }

   /* public static int getID() {
        return ID;
    }

    public static void setID(int ID) {
        Libro.ID = ID;
    }
*/
    public int getCodLib() {
        return codLib;
    }

    public void setCodLib(int codLib) {
        this.codLib = codLib;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getCantEjemplar() {
        return cantEjemplar;
    }

    public void setCantEjemplar(int cantEjemplar) {
        this.cantEjemplar = cantEjemplar;
    }

  /*  public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }
*/
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }
/*
    @Override
    public String toString() {
        return " " + this.getCodLib() + "               " + nombre + "             "
                + estado + "               "
                + anio + "            " + categoria + "             " + editorial + "                " + cantEjemplar + "\n\n";
    }

    public void mostrar() {
        EntradaSalida.mostrarString(this.toString());
    }
    */
    public void disminuirEjemplar() {
        this.cantEjemplar--;
    }
    

    
    
    
    
}
