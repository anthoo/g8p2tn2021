/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import java.util.List;
import misClases.Libro;

/**
 *
 * @author Brigitte
 */
public interface CrudLibro {
     public List listar();
    public Libro list(int id);
    public boolean add(Libro lib);
    public boolean edit(Libro lib);
    public boolean eliminar(int id);
    
}
